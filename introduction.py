#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pandas as pd


# Load data into Pandas

# In[10]:


# Using a data frame (multi-dimensional array)
# Load data from a csv file. Use read_csv('/path/to/file')
# print(df) shows all the data read from the csv
# To view only a subset, say, first 3 rows, use: print(df.head(number_of_head_rows)) and print(df.tail(number_of_bottom_rows))

df = pd.read_csv('G:\Data Science Projects\Pandas 101\pokemon_data.csv')
print(df.head(3))

# Load data from an xlsx file. Use pd.read_excel('/path/to/file')
df_excel = pd.read_excel('G:\Data Science Projects\Pandas 101\pokemon_data.xlsx')
#print(df_excel.tail(3))

# Load data from a txt file (tab-separated values. Use pd.read_csv('/path/to/file', delimiter='\t')
# Note to ALWAYS SPECIFY THE USED DELIMITER FOR TXT!!
df_txt = pd.read_csv('G:\Data Science Projects\Pandas 101\pokemon_data.txt', delimiter='\t')
#print(df_txt.head(2))


# Reading Data into Pandas

# In[11]:


# Read headers
df.columns


# In[18]:


# Read each column
print(df['Name'][0:5])

# Read multiple columns
# Use a list definition
df[['Name', 'Type 1', 'Type 2']][0:5]


# In[16]:


# Read each row
# Use df.iloc[row index - starts from 0 to r-1]
# iloc stands for 'index location'
print(df.iloc[0])


# In[17]:


# Read multiple rows
# Use df.iloc[range e.g. 0:6 for the first 5 rows]

df.iloc[0:6]


# In[21]:


# Read a specific location
# Specify (R,C) in iloc[], e.g. df.iloc[2,1] to get the Name 'Venusaur'

print(df.iloc[2,1])


# In[22]:


# Iterate through each row as you read it and access any data that you want:
for index, row in df.iterrows():
    print(index, row)


# In[32]:


# Customize the iteration to get specific rows:
# For more than one row, define a list e.g. print(index, row[['Name','Speed']])
# For only one, pass it directly as an accessible e.g. print(index, row['Name'])

for index, row in df.iterrows():
    print(index, row[['Name','Speed']])


# In[33]:


# Get a number of rows that contain a certain value
# Use df.loc[df['col_name']=="ContainsThis"]:

df.loc[df['Type 2'] == "Poison"]


# In[ ]:




