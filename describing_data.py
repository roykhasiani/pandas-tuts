#!/usr/bin/env python
# coding: utf-8

# ### Describing Data

# In[1]:


import numpy as np
import pandas as pd


# Get simple statistical analyses - Some not useful

# In[3]:


# Use df.describe()
df = pd.read_csv('G:\Data Science Projects\Pandas 101\pokemon_data.csv')
df.describe()


# Sort in ascending or descending based on a column

# In[9]:


# Use sort_values('Column_Name', ascending=False/0) to sort in descending order based on Column_Name
# Use sort_values('Column_Name') to sort in ascending order based on Column_Name

print (df.sort_values('Name', ascending=0))

# Other cool sort stuff
# print(df.sort_values(['Name', 'HP'], ascending=[1,0])) #sorts Name ascending and HP descending


# ### Mutating the Data

# In[10]:


# Create a new column and populate it with sum of values of other columns
# Many ways to do it.

df['Total'] = df['HP']+df['Attack']+df['Defense']+df['Sp. Atk']+df['Sp. Def']+df['Speed']
df.head(3)


# In[15]:


# Drop columns

df
df = df.drop(columns=['Legendary'])
print(df)


# In[22]:


# Add columns more succinctly
# Pass axis=1 in sum() to add horizontally

df['Total'] = df.iloc[:,4:10].sum(axis=1)
print(df.head(3))


# ### Exporting to .csv, .xlsx, .txt

# In[24]:


# Use built-in functions to_csv(), to_excel(). Remove the index column by setting it to False

# Save to csv
df.to_csv('modified_pokemon_data.csv', index=False)


# In[25]:


# Save to excel
df.to_excel('modified_pokemon_data.xlsx', index=False)


# In[27]:


# Save to txt separated by Tabs
df.to_csv('modified_pokemon_data.txt', index=False, sep='\t')


# In[ ]:




